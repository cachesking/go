package bbotclient

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const BaseUrl = "https://mygeoangelfirespace.city"

type Command struct {
	Name      string   `json:"name"`
	User      string   `json:"user"`
	Permitted []string `json:"permitted_user"`
	Health    int      `json:"health"`
	Cost      int      `json:"Cost"`
}

type Commands struct {
	Stats map[string]Command `json:"commands"`
}

type User struct {
	Name       string   `json:"name"`
	StreetCred int      `json:"street_cred"`
	CoolPoints int      `json:"cool_points"`
	RideOrDie  string   `json:"ride_or_die"`
	Css        string   `json:"custom_css"`
	Notoriety  int      `json:"notoriety"`
	TopEight   []string `json:"top_eight"`
	Creator    string   `json:"creator"`
	IsBot      bool     `json:"is_bot"`
}

type Users struct {
	Stats map[string]User `json:"users"`
}

type CubeStats struct {
	Stats map[string]Stats `json:"cube_stats"`
}

type Bet struct {
	User string `json:"user"`
	Bet  int    `json:"bet"`
}

type Stats struct {
	AllBets  []Bet    `json:"all_bets"`
	Duration int      `json:"winning_duration"`
	Winners  []string `json:"winners"`
}

type Client struct{}

func New() *Client {
	return &Client{}
}

func (c *Client) CubeStats() (*CubeStats, error) {
	res, err := http.Get(fmt.Sprintf("%s/db/cube_stats.json", BaseUrl))
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	var ret CubeStats
	if err := json.NewDecoder(res.Body).Decode(&ret); err != nil {
		return nil, err
	}
	return &ret, nil
}

func (c *Client) Users() (*Users, error) {
	res, err := http.Get(fmt.Sprintf("%s/db/users.json", BaseUrl))
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	var ret Users
	if err := json.NewDecoder(res.Body).Decode(&ret); err != nil {
		return nil, err
	}
	return &ret, nil
}

func (c *Client) Commands() (*Commands, error) {
	res, err := http.Get(fmt.Sprintf("%s/db/users.json", BaseUrl))
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	var ret Commands
	if err := json.NewDecoder(res.Body).Decode(&ret); err != nil {
		return nil, err
	}
	return &ret, nil
}
