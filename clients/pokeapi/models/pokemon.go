package models

type APIResource struct {
	Url string `json:"url"`
}

type NamedAPIResource struct {
	Name string `json:"name"`
}

type PokemonAbility struct {
	IsHidden bool             `json:"is_hidden"`
	Slot     int              `json:"slot"`
	Ability  NamedAPIResource `json:"ability"`
}

type PokemonType struct {
	Slot int              `json:"slot"`
	Type NamedAPIResource `json:"type"`
}

type PokemonMove struct {
	Move NamedAPIResource `json:"move"`
}

type PokemonStat struct {
	Stat     NamedAPIResource `json:"stat"`
	Effort   int              `json:"effort"`
	BaseStat int              `json:"base_stat"`
}

type Pokemon struct {
	ID        int                `json:"id"`
	Name      string             `json:"name"`
	Height    int                `json:"height"`
	Weight    int                `json:"weight"`
	IsDefault bool               `json:"is_default"`
	Order     int                `json:"order"`
	Abilities []PokemonAbility   `json:"abilities"`
	Forms     []NamedAPIResource `json:"forms"`
	Moves     []PokemonMove      `json:"moves"`
	Species   NamedAPIResource   `json:"species"`
	Stats     []PokemonStat      `json:"stats"`
	Types     []PokemonType      `json:"types"`
}
