package pokeapi

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/patrickmn/go-cache"
)

const uri = "https://pokeapi.co/api/v2"

var c *cache.Cache

func init() {
	c = cache.New(5*time.Minute, 10*time.Minute)
}

func do(resource string, v interface{}) error {
	cached, found := c.Get(resource)
	if found {
		return json.Unmarshal(cached.([]byte), &v)
	}

	req, err := http.NewRequest(http.MethodGet, uri+resource, nil)
	if err != nil {
		return err
	}

	client := &http.Client{Timeout: 10 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	c.SetDefault(resource, body)
	return json.Unmarshal(body, &v)
}
