package pokeapi

import (
	"gitlab.com/cachesking/go/clients/pokeapi/models"
)

func Pokemon(id string) (p models.Pokemon, err error) {
	err = do("/pokemon/"+id, &p)
	return p, err
}
