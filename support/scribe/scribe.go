package scribe

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gopkg.in/natefinch/lumberjack.v2"
	"io"
	"os"
	"path"
)

type Level int8

type Config struct {
	LogLevel    Level
	ConsoleLogs bool
	FileLogs    bool
	JsonEncode  bool
	Directory   string
	Filename    string
	// Size in MB before
	MaxSize int
	// Number of rolled files
	MaxBackups int
	// Days to keep log
	MaxAge int
}

type Scribbler struct {
	*zerolog.Logger
}

func NewConsoleScribbler() *Scribbler {
	return Configure(Config{ConsoleLogs: true})
}

// Configure news up the logger's framework
func Configure(c Config) *Scribbler {
	var writers []io.Writer

	if c.ConsoleLogs {
		writers = append(writers, zerolog.ConsoleWriter{Out: os.Stderr})
	}

	if c.FileLogs {
		writers = append(writers, newLogRoller(c))
	}

	mw := io.MultiWriter(writers...)

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	logger := zerolog.New(mw).With().Timestamp().Logger()

	return &Scribbler{
		Logger: &logger,
	}
}

func newLogRoller(c Config) io.Writer {
	if err := os.MkdirAll(c.Directory, 0744); err != nil {
		log.Err(err).Str("path", c.Directory).Msg("")
		return nil
	}

	return &lumberjack.Logger{
		Filename:   path.Join(c.Directory, c.Filename),
		MaxBackups: c.MaxBackups,
		MaxSize:    c.MaxSize,
		MaxAge:     c.MaxAge,
	}
}
